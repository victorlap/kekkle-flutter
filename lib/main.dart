import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:kekkle/Pages/webview.dart';
import 'package:sentry_flutter/sentry_flutter.dart';

import 'Values/colors.dart';
import 'firebase_options.dart';

final webviewKey = GlobalKey<MyWebViewState>();

Future<void> main() async {
  WidgetsBinding widgetsBinding = WidgetsFlutterBinding.ensureInitialized(); // initialize the binding
  FlutterNativeSplash.preserve(widgetsBinding: widgetsBinding);
  try {
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );
  } catch (e) {
    Sentry.captureException(e);
  }

  await SentryFlutter.init(
    (options) {
      options.dsn = kReleaseMode ? 'https://6b2d4c663a044d889a4bbb0cdb1c44ec@o922252.ingest.sentry.io/4505149765255168' : '';
      options.enableTracing = false;
    },
    appRunner: () => runApp(const MyApp()),
  );
  FlutterNativeSplash.remove();
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => MyMainState();
}

class MyMainState extends State<MyApp> {
  late MyWebView webView;

  @override
  void initState() {
    super.initState();
    webView = MyWebView(key: webviewKey);
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: kYellowMain,
      initialRoute: '/',
      routes: {
        '/': (context) => webView,
      },
      onGenerateRoute: (settings) {
        if (webviewKey.currentState != null) {
          webviewKey.currentState?.changeUrl(settings.name.toString());
        } else {
          //set an interval that repeats until the webview is initialized
          Timer.periodic(const Duration(milliseconds: 20), (Timer t) {
            if (webviewKey.currentState != null) {
              t.cancel();
              webviewKey.currentState?.changeUrl(settings.name.toString());
            }
          });
        }
        return null;
      },
    );
  }
}
