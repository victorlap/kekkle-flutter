import 'dart:ui';

const kYellowLight = Color(0xFFFFE161);
const kYellowMain = Color(0xFFF8D112);
const kYellowDark = Color(0xFFD3AB02);

const kBlackLight = Color(0xFF485159);
const kBlackMain = Color(0xFF292F36);
const kBlackDark = Color(0xFF1D252D);

const kBlueLight = Color(0xFF39E8DF);
const kBlueMain = Color(0xFF4ECDC4);
const kBlueDark = Color(0xFF36B2A6);

const kRedLight = Color(0xFFFFA4A4);
const kRedMain = Color(0xFFFF6B6B);
const kRedDark = Color(0xFFE84242);

const kPurpleLight = Color(0xFFCCA1CE);
const kPurpleMain = Color(0xFFA677A6);
const kPurpleDark = Color(0xFF7E527F);
