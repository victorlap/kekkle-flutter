import 'dart:io' show File;

import 'package:file_picker/file_picker.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart' as webview_flutter_android;

Future<List<String>> androidFilePicker(webview_flutter_android.FileSelectorParams params) async {
  try {
    if (params.mode == webview_flutter_android.FileSelectorMode.openMultiple) {
      final attachments = await FilePicker.platform.pickFiles(allowMultiple: true, type: FileType.image);
      if (attachments == null) return [];

      return attachments.files.where((element) => element.path != null).map((e) => File(e.path!).uri.toString()).toList();
    } else {
      final attachment = await FilePicker.platform.pickFiles(allowMultiple: false, type: FileType.image);
      if (attachment == null) {
        return [];
      }
      File file = File(attachment.files.first.path!);
      return [file.uri.toString()];
    }
  } catch (e) {
    Sentry.captureException(e);
    return [];
  }
}
