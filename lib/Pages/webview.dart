import 'dart:convert';
import 'dart:io' show Directory, File, Platform;

import 'package:cloud_functions/cloud_functions.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gallery_saver/gallery_saver.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:kekkle/Values/colors.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_android/webview_flutter_android.dart' as webview_flutter_android;

import '../Helpers/file_picker.dart';

const String productionUrl = 'https://app.kekkle.nl';
const String developmentUrl = 'https://development.kekkle-app.pages.dev';
const String localUrl = 'http://localhost:4173';
const String localUrlAndroid = 'http://10.0.2.2:4173';

class MyWebView extends StatefulWidget {
  const MyWebView({required Key key}) : super(key: key);

  @override
  State<MyWebView> createState() => MyWebViewState();
}

class MyWebViewState extends State<MyWebView> with WidgetsBindingObserver {
  late WebViewController controller;
  late NavigationDelegate navigationDelegate;
  static const Key webViewWidgetKey = Key('WebViewWidgetKey');

  //CHANGE TO CORRECT URL
  String baseURL = productionUrl;
  String initUrl = productionUrl;
  String changedUrl = '';
  bool showLoadingSpinner = true;
  bool isDarkMode = false;
  int historyLength = -1;
  bool wentIntoBackground = false;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    super.didChangeAppLifecycleState(state);

    switch (state) {
      case AppLifecycleState.inactive:
        //Do nothing
        break;
      case AppLifecycleState.paused:
        //Went into background
        setState(() {
          wentIntoBackground = true;
        });
        break;
      case AppLifecycleState.hidden:
        //Went into background
        setState(() {
          wentIntoBackground = true;
        });
        break;
      case AppLifecycleState.detached:
        //Went into background and is cleared from memory
        controller.loadRequest(Uri.parse('about:blank'));
        setState(() {
          wentIntoBackground = true;
        });
        break;
      case AppLifecycleState.resumed:
        //if did not go into background, do nothing
        if (!wentIntoBackground) return;
        //if went into background, check if the url has changed
        try {
          setState(() => showLoadingSpinner = true);
          var currentUrl = await controller.currentUrl();
          if (currentUrl == null || currentUrl.isEmpty || currentUrl == 'about:blank') {
            //if the current url is empty, load the initial url
            controller.loadRequest(Uri.parse(initUrl));
          }
        } catch (error) {
          Sentry.captureException(error);
          //if there is an error, load the initial url
          controller.loadRequest(Uri.parse(initUrl));
        } finally {
          await Future.delayed(const Duration(milliseconds: 100));
          setState(() {
            showLoadingSpinner = false;
            wentIntoBackground = false;
          });
        }
        break;
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    controller.loadRequest(Uri.parse('about:blank'));
    super.dispose();
  }

  @override
  void initState() {
    FirebaseMessaging.instance.onTokenRefresh.listen((fcmToken) {
      sendFcmTokenToWeb(fcmToken: fcmToken);
    }).onError((err) {});

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      var title = message.notification?.title;
      var link = message.data['link'];
      if (title != null) {
        showWebToast(type: 'info', message: title, link: link ?? '');
      }
    });

    setupInteractedMessage();
    navigationDelegate = NavigationDelegate(
      onPageStarted: onPageStarted,
      onPageFinished: pageFinished,
      onNavigationRequest: onNavigationRequest,
    );
    controller = WebViewController();
    controller.addJavaScriptChannel('FlutterClipboard', onMessageReceived: copyToClipboard);
    controller.addJavaScriptChannel('FlutterSaveFile', onMessageReceived: saveFile);
    controller.addJavaScriptChannel('FlutterFcm', onMessageReceived: getFlutterFcmToken);
    controller.addJavaScriptChannel('FlutterAppleId', onMessageReceived: getAppleIdToken);
    controller.addJavaScriptChannel('FlutterGoogleId', onMessageReceived: getGoogleIdToken);
    controller.addJavaScriptChannel('FlutterLogoutAuthInstance', onMessageReceived: logoutFlutterAuthInstance);
    controller.addJavaScriptChannel('FlutterShare', onMessageReceived: openNativeShare);
    controller.setJavaScriptMode(JavaScriptMode.unrestricted);
    controller.setUserAgent('random');
    controller.setBackgroundColor(const Color(0x00000000));
    controller.setNavigationDelegate(navigationDelegate);
    controller.loadRequest(Uri.parse(initUrl));
    initFilePicker();
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  Future<NavigationDecision> onNavigationRequest(NavigationRequest request) async {
    if (request.url.contains('ko-fi') || request.url.contains('twitter') || request.url.contains('docs.google')) {
      launchUrl(Uri.parse(request.url));
      return NavigationDecision.prevent;
    } else {
      return NavigationDecision.navigate;
    }
  }

  void onPageStarted(String url) {
    controller.runJavaScript('window.flutter = true; window.nativeAppleAuthSupported = true; window.nativeGoogleAuthSupported = true; window.nativeShareSupported = true');
  }

  void pageFinished(String url) {
    controller.runJavaScript('window.flutter = true; window.nativeAppleAuthSupported = true; window.nativeGoogleAuthSupported = true; window.nativeShareSupported = true');
    setState(() {
      showLoadingSpinner = false;
      historyLength++;
    });
  }

  setupInteractedMessage() async {
    // Get any messages which caused the application to open from
    // a terminated state.
    RemoteMessage? initialMessage = await FirebaseMessaging.instance.getInitialMessage();

    // If the message also contains a data property with a "type" of "chat",
    // navigate to a chat screen
    if (initialMessage != null) {
      var link = initialMessage.data['link'];
      if (link != null) {
        changeUrl(link);
      }
    }
    // Also handle any interaction when the app is in the background via a
    // Stream listener
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      var link = message.data['link'];
      if (link != null) {
        changeUrl(link);
      }
    });
  }

  showWebToast({required String type, required String message, String? link}) {
    if (link != null) {
      controller.runJavaScript('window.flutterFunctions.setToast("$type", "$message", "$link")');
    } else {
      controller.runJavaScript('window.flutterFunctions.setToast("$type", "$message")');
    }
  }

  changeUrl(String url) {
    changedUrl = baseURL + url;
    controller.loadRequest(Uri.parse(changedUrl));
  }

  sendFcmTokenToWeb({required String fcmToken}) {
    controller.runJavaScript('window.flutterFunctions.postFcmToken("$fcmToken")');
  }

  sendNotificationPermissionStatus({required bool notificationGranted}) {
    controller.runJavaScript('window.flutterFunctions.setNotificationPermissionStatus("$notificationGranted")');
  }

  sendCustomIdTokenToWeb({required String customIdToken}) {
    controller.runJavaScript('window.flutterFunctions.setCustomIdToken("$customIdToken")');
  }

  sendCancelAppleAuthToWeb(String message) {
    controller.runJavaScript('window.flutterFunctions.cancelAppleAuth("$message")');
  }

  sendCancelGoogleAuthToWeb(String message) {
    controller.runJavaScript('window.flutterFunctions.cancelGoogleAuth("$message")');
  }

  // receiving messages from the webview and handling them

  getFlutterFcmToken(JavaScriptMessage message) async {
    var status = await Permission.notification.status;
    if (!status.isGranted) {
      status = await Permission.notification.request();
    }
    sendNotificationPermissionStatus(notificationGranted: status.isGranted);
    try {
      final fcmToken = await FirebaseMessaging.instance.getToken();
      if (fcmToken != null) {
        sendFcmTokenToWeb(fcmToken: fcmToken);
      }
    } catch (e) {
      //
    }
  }

  Future<String?> fetchCustomAuthTokenFromFirebase() async {
    try {
      final functions = FirebaseFunctions.instanceFor(region: 'europe-west3');
      final result = await functions.httpsCallable('getCustomAuthToken').call();
      return result.data as String;
    } catch (e) {
      Sentry.captureException(e);
      return null;
    }
  }

  warmupFirebaseCustomAuthTokenFunction() {
    // Warm up the function to prevent cold starts
    final functions = FirebaseFunctions.instanceFor(region: 'europe-west3');
    functions.httpsCallable('getCustomAuthToken').call().then((value) => null).catchError((e) => null);
  }

  getAppleIdToken(JavaScriptMessage message) async {
    warmupFirebaseCustomAuthTokenFunction();
    try {
      final appleProvider = AppleAuthProvider();
      appleProvider.addScope('email');
      await FirebaseAuth.instance.signInWithProvider(appleProvider);
    } catch (e) {
      Sentry.captureException(e);
      sendCancelAppleAuthToWeb('Failed to sign in with Apple');
      return;
    }
    if (FirebaseAuth.instance.currentUser == null) {
      Sentry.captureMessage('Flutter firebaseAuth.currentUser is null during Apple sign in');
      sendCancelAppleAuthToWeb('Failed to sign in with Apple');
      return;
    }
    final customAuthIdToken = await fetchCustomAuthTokenFromFirebase();
    if (customAuthIdToken == null) {
      sendCancelAppleAuthToWeb('Failed to get custom auth token');
      return;
    }
    sendCustomIdTokenToWeb(customIdToken: customAuthIdToken);
  }

  getGoogleIdToken(JavaScriptMessage message) async {
    warmupFirebaseCustomAuthTokenFunction();
    try {
      final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();
      // Obtain the auth details from the request
      final GoogleSignInAuthentication? googleAuth = await googleUser?.authentication;
      // Create a new credential
      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth?.accessToken,
        idToken: googleAuth?.idToken,
      );
      // Once signed in, return the UserCredential
      await FirebaseAuth.instance.signInWithCredential(credential);
      if (FirebaseAuth.instance.currentUser == null) {
        Sentry.captureMessage('Flutter firebaseAuth.currentUser is null during Google sign in');
        sendCancelGoogleAuthToWeb('Failed to sign in with Google');
        return;
      }
      final customAuthIdToken = await fetchCustomAuthTokenFromFirebase();
      if (customAuthIdToken == null) {
        sendCancelGoogleAuthToWeb('Failed to get custom auth token');
        return;
      }
      sendCustomIdTokenToWeb(customIdToken: customAuthIdToken);
    } catch (e) {
      Sentry.captureException(e);
      sendCancelGoogleAuthToWeb('Failed to sign in with Google');
      return;
    }
  }

  logoutFlutterAuthInstance(JavaScriptMessage message) async {
    try {
      GoogleSignIn googleSignIn = GoogleSignIn();
      await googleSignIn.disconnect();
    } catch (e) {
      Sentry.captureException(e);
    }

    if (FirebaseAuth.instance.currentUser != null) {
      try {
        await FirebaseAuth.instance.signOut();
      } catch (e) {
        Sentry.captureException(e);
      }
    }
    try {
      await FirebaseMessaging.instance.deleteToken();
    } catch (e) {
      Sentry.captureException(e);
    }
  }

  copyToClipboard(JavaScriptMessage message) async {
    var decoded = jsonDecode(message.message);
    String text = decoded['data'];
    String successMessage = decoded['successMessage'];
    await Clipboard.setData(ClipboardData(text: text));
    showWebToast(type: "success", message: successMessage);
  }

  saveFile(JavaScriptMessage message) async {
    var data = jsonDecode(message.message);
    String fileFolder = data['fileFolder'];
    String fileName = data['fileName'] ?? 'caption';
    String successMessage = data['successMessage'];
    String filePath = '';

    try {
      final base64String = data['data'].replaceFirst(RegExp(r'data:image/[^;]+;base64,'), '');
      Uint8List decodedBytes = base64Decode(base64String);
      final fileExtension = data['data'].split(',')[0].split(':')[1].split(';')[0].split('/')[1];
      Directory directory = await getApplicationDocumentsDirectory();
      String groupDirectoryPath = '${directory.path}/$fileFolder';
      if (await Directory(groupDirectoryPath).exists()) {
        // The group directory already exists
        filePath = '$groupDirectoryPath/$fileName.$fileExtension';
      } else {
        // The group directory doesn't exist, create it
        Directory(groupDirectoryPath).createSync(recursive: true);
        filePath = '$groupDirectoryPath/$fileName.$fileExtension';
      }

      File file = File(filePath);
      // Uint8List decodedBytes = base64Decode(data['data']);

      await file.writeAsBytes(decodedBytes);
      await Permission.storage.request();

      try {
        await GallerySaver.saveImage(file.path, albumName: fileFolder);
        showWebToast(type: 'success', message: successMessage);
      } catch (e) {
        showWebToast(type: 'error', message: 'No file permission');
      }
    } catch (e) {
      Sentry.captureException(e);
    }
  }

  openNativeShare(JavaScriptMessage message) async {
    var decoded = jsonDecode(message.message);
    String title = decoded['title'];
    String subject = decoded['subject'];
    final box = context.findRenderObject() as RenderBox?;
    if (subject.isEmpty) {
      await Share.share(
        title,
        sharePositionOrigin: box!.localToGlobal(Offset.zero) & box.size,
      );
    } else {
      await Share.share(
        title,
        subject: subject,
        sharePositionOrigin: box!.localToGlobal(Offset.zero) & box.size,
      );
    }
  }

  initFilePicker() async {
    if (Platform.isAndroid) {
      final androidController = (controller.platform as webview_flutter_android.AndroidWebViewController);
      await androidController.setOnShowFileSelector(androidFilePicker);
    }
  }

  @override
  Widget build(BuildContext context) {
    isDarkMode = MediaQuery.of(context).platformBrightness == Brightness.dark;
    if (showLoadingSpinner) {
      return Container(
        color: isDarkMode ? kBlackLight : kYellowMain,
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 50,
              height: 50,
              child: Image.asset(isDarkMode ? 'lib/Assets/logo-trans-yellow.png' : 'lib/Assets/logo-trans-alt.png'),
            ),
            const SizedBox(height: 20),
            if (isDarkMode)
              const CircularProgressIndicator(
                color: kYellowMain,
              ),
            if (!isDarkMode)
              const CircularProgressIndicator(
                color: kBlackDark,
              ),
          ],
        )),
      );
    } else {
      return PopScope(
          canPop: historyLength < 1,
          onPopInvoked: (bool didPop) async {
            controller.goBack();
            bool canGoBack = await controller.canGoBack();
            if (!canGoBack) {
              setState(() {
                historyLength = 0;
              });
            }
            return;
          },
          child: Container(
            color: kYellowMain,
            child: SafeArea(
                child: Padding(
              padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
              child: WebViewWidget(
                key: webViewWidgetKey,
                controller: controller,
              ),
            )),
          ));
    }
  }
}
