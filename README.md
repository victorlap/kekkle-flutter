# Kekkle Flutter 📱

This is the Flutter wrapper of the Kekkle project. 
This project is a minimal Flutter project that loads a webview in the app with some custom logic to call dart code from the webview.    


### Related Repositories

Check out other parts of the Kekkle project:

- ☁️ [Kekkle Firebase cloud functions](https://gitlab.com/samfreriks/kekkle-functions)
- 🌐 [Kekkle Vue.js Frontend](https://gitlab.com/samfreriks/kekkle-front)
- 🏠 [Kekkle Homepage](https://gitlab.com/samfreriks/kekkle-home)

## Development 
### Setup project

- run `fvm use` to use the correct flutter version
- run `wich flutter` to check if the correct flutter version is used, this can be an alias to `fvm flutter`
- run `xcodes select` to select the correct xcode version
- run `flutter doctor` to check if everything is installed correctly
- run `flutter pub get` to install all dependencies

### Local development
- Change the url in the `webview.dart` to the desired location.

## Usefull commands
#### Flutter upgrade
- run `fvm releases` to check if there is a new version available
- run `fvm install VERSION` to install the latest version
- run `fvm use VERSION` to use the latest version


#### Coacoapods
- Run `brew install cocoapods` to install cocoapods
- Run `pod repo update` to update the cocoapods repo


#### Upgrading packages
run `flutter pub upgrade` to upgrade all packages
running `flutter pub add 'package name'` when a package is already installed will upgrade the package to the latest compatible version. You can run `flutter pub upgrade --tighten` to also update the pubspec.yml

#### Simulator
run `flutter devices` to check if the simulator is available
You can download the simulator from xcode, or use the simulator from android studio.


#### Run project

run `flutter run` to run the project on the simulator

## troubleshooting
run `flutter clean` to clean the project
run `flutter pub get` to install all dependencies

### Firebase
This project uses firebase for authentication, and messaging.

run `flutterfire configure` to update the firebase config
run `flutterfire update` to update the firebase packages

## List before building production app
- [ ] Set correct URL in webview.dart
- [ ] Set correct firebaseconfig with `flutterfire configure`
- [ ] Increase build number and version


## Contributing

Do you want to contribute? Here's how:

1. Create an issue on GitLab.
2. Create a new branch based on master.
3. Submit a Merge Request.
4. Wait for approval.
5. Profit! 🎉